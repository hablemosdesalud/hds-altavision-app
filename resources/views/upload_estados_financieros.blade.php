<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin 2 - Bootstrap Admin Theme</title>
  <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
  <link href="css/dashboard/sb-admin-2.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="container">
  @include('requests')
  @include('flash::message')
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="login-panel panel panel-green">
        <div class="panel-heading">
          <h3 class="panel-title">Subir archivo de estados financieros</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="form-group">
              <div class="col-xs-10 text-justify">
                <p> <b>1) </b>Agregue un archivo en PDF con nombre del año correspondiente asi: 2018.pdf</p>
                <p> <b>2) </b>Para ver el estado financiero actual visitar: <a href="http://www.altavision.co/estados-financieros" target="_blank">altavision.co/estados-financieros</a></p>
                <p> <b>3) </b>Para consultar el estado financiero de un año en específico por ejemplo 2017 visitar: altavision.co/estados-financieros/2017</p>
              </div>
            </div>
            <div class="col-xs-6">
              {!!Form::open(['action'=>'FrontController@upload_file' , 'method'=>'POST' , 'class'=>'formulario' , 'enctype'=>'multipart/form-data'])!!}
                <div class="form-group text-right">
                  {!! Form::file('file') !!}
                </div>
                <div class="form-group">
                  {!!Form::submit('Subir archivo' , ['class'=>'btn btn-success'])!!}
                </div>
              {!!Form::close()!!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="js/bootstrap/jquery.min.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/dashboard/metisMenu.min.js"></script>
<script src="js/dashboard/sb-admin-2.js"></script>
</body>
</html>