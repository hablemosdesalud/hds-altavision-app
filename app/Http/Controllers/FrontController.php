<?php

namespace altavisionApp\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use altavisionApp\Review;
use altavisionApp\Http\Requests;
use altavisionApp\Http\Controllers\Controller;

class FrontController extends Controller
{
    public function __construct(){
        $this->middleware('auth' , ['only'=>['admin' , 'uploadEstadosFinancieros']]);
    }
    //////////////////////////////////////////////
    public function index()
    {
      $reviews=Review::all();
      return view('index', ['reviews'=>$reviews]);
    }
    //////////////////////////////////////////////
    public function admin()
    {
      return view('dashboard');
    }
    //////////////////////////////////////////////
    public function whatsapp()
    {
      return view('whatsapp');
    }
    //////////////////////////////////////////////
    public function getEstadosFinancieros($year = null)
    {
        if (!$year)
        {
            $year = Carbon::now()->format('Y');
        }
      return view('estados_financieros' , array('year' => $year));
    }
    //////////////////////////////////////////////
    public function uploadEstadosFinancieros()
    {
        return view('upload_estados_financieros');
    }
    public function upload_file(Request $request){

        if ($request->hasFile('file'))
        {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $route = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
            $file->move('pdf' , $name );
        }
        return redirect('estados-financieros/'.$route);
    }
}
